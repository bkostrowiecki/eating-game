/// <reference path="../../node_modules/phaser/typescript/phaser.d.ts" />

export enum HeadMoodState {
    CALM = 0,
    CONSUMING = 1,
    SAD = 2,
    HAPPY = 3
}