/// <reference path="../node_modules/phaser/typescript/phaser.d.ts" />

export enum Reaction {
    NONE = 0,
    GOOD = 1,
    BAD = 2
};